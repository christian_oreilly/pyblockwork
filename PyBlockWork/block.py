# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""


from abc import ABCMeta, abstractmethod 
from copy import deepcopy
from cStringIO import StringIO
import uuid
import serpent
import traceback

from .stagingRoom import StagingRoomWrapper
from .registerWrapper import RegisterWrapper
from .pyroWarehouse import warehouse
    
__PROFILING___ = True
    
    
class Block():
    __metaclass__ = ABCMeta
    
    """
     Note: This class used to be a subclass of Process and containt the Pyro4 
     logic. This part of the core has been migrated in the worker class.
    
     inputs  : Is a dictionnary of variableLabels:variableValues. Big objects
               should be pickled and only the file name should be passed because
               input objects are used in the object signature.
     outputs : A dictionnaly of output variableLabels:outputProperties:
               "parallel": for an object that which all elements can be proceed
                           independently.
               "atomic"  : for an object that which...
    """
    def __init__(self, inputs, outputs, verbose = True, ID = None, name = "block"):

        self.name=name

        for inputKey in inputs:
            self.__dict__[inputKey] = inputs[inputKey]

        if ID is None:
            self.ID = str(uuid.uuid1())
        else:
            self.ID = ID

        self.verbose  = verbose
        self.outputs  = outputs
        self.depth    = 0

        # We keep this as it is used in buliding the signature of the block.
        self.inputs = inputs

        self.nextBlockIDs       = []
        self.previousBlockIDs   = []

        self.register           = None
        self.stagingRoom        = None        

        self.registerName       = None
        self.stagingRoomName    = None
        self.comHubName         = None
        self.HMAC_KEY           = None

    
    def setForkID(self, idString):
        """
         From execution to execution, corresponding blocks should have the 
         same ID, else refering to blocks from execution to execution using ID
         is impossible. IDs of blocks of the canonical schema are obtained at
         the construction of the shema using uuid.uuid1() and are saved in the
         schema so these blocks do not cause any problem. For blocks created by
         duplication using the duplicateBranch() method and created at every 
         execution time, the uuid.uuid1() function cannot be
         used because it would generate different IDs at each execution. We 
         therefore use a different strategy, taking the uuid.uuid5() function and
         passing the ID of the canonical sibiling as first parameter and a 
         string that differentiate this branch from the others as the second 
         parameter. This string is the input values of the sibling block at the 
         branching point. 
         
         The ID of the canonical block does not need to be passed as parameter
         as it is copied in the siblings ID field during the duplication
         operation.
        """
        self.ID = str(uuid.uuid5(uuid.UUID(self.ID), idString))



    def resetStatus(self):        
        if self.register is None:
            self.register = RegisterWrapper(self.registerName)      
            
        self.register.resetStatus(self)



    def getNextBlocks(self, recursive=True) :
        if recursive:
            if self.register is None:
                self.register = RegisterWrapper(self.registerName)                   
            
            nextBlocks = deepcopy(self.nextBlockIDs)
            for ID in self.nextBlockIDs:
                block = self.register.getBlock(ID) 
                nextBlocks.extend(block.getNextBlocks(True))
            return nextBlocks

        else:
            return self.nextBlockIDs


    def addNextBlock(self, block):
        """
        This method should be used to add a block that must be executed after
        the termination of the current blocks. 
        """
        
        if self.register is None:
            self.register = RegisterWrapper(self.registerName)           
                    
        if block.ID in self.nextBlockIDs:
            raise ValueError("Block ID already in next block ID list.")
        if self.ID in block.previousBlockIDs:
            raise ValueError("Block ID already in previous block ID list.")

        #self.print_(str(self.name) + ":" + str(self.ID) + " next " + str(block.name) + ":" + str(block.ID))
        self.nextBlockIDs.append(block.ID)        
        block.depth = self.depth+1                   
        block.previousBlockIDs.append(self.ID)  
        
        if self.register.isValid():
            self.register.updateBlock(self)
            self.register.updateBlock(block)           


    @property
    def comHub(self):
        return warehouse.proxy(self.comHubName)        


    def removeNextBlock(self, block):
        """
        This method should be used to add a block that must be executed after
        the termination of the current blocks. 
        """
            
        if self.register is None:
            self.register = RegisterWrapper(self.registerName)               
            
        if not block.ID in self.nextBlockIDs:
            raise ValueError("Block ID is not in next block ID list.")
        if not self.ID in block.previousBlockIDs:
            raise ValueError("Block ID is not in previous block ID list.")

        #self.print_(str(self.name) + ":" + str(self.ID) + " next " + str(block.name) + ":" + str(block.ID))
        self.nextBlockIDs.remove(block.ID)        
        block.previousBlockIDs.remove(self.ID)        
        self.register.updateBlock(self)
        self.register.updateBlock(block)           




    @abstractmethod
    def execute(self): 
        """
         Should be implemented with the code to execute when this block is 
         processed by a worker. In this code, input and
         output variables must be preceded by "self."
        """        
        raise NotImplementedError("Block.execute() is an abstract method."\
                                  " It must be implemented in classes derived"\
                                  " from the Block class.")


    @abstractmethod
    def rollback(self): 
        """
         Should be implemented in subclasses with the code necessary to undo 
         any persistent change that has been made when executing the block 
         (if it has been executed yet). The subclass should not forget to call
         self.resetStatus() when implementing this function such that the 
         block status are reset.
        """ 
        raise NotImplementedError("Block.rollback() is an abstract method."\
                                  " It must be implemented in classes derived"\
                                  " from the Block class.")



    """
     Return the signature of the BWM.
    """
    def getSignature(self):
        signature = str(type(self)) + ":" + str(self.inputs) + ":" + str(self.outputs)
        return signature


    def print_(self, string):        
        self.comHub.writeMessage(self.workerID, string, self.ID)  



    """
     The function used to start the processing of the block. Normally, it should
     by a BlockWorker object.
    """
    def run(self, workerID, registerName=None, stagingRoomName=None, 
                                comHubName=None): 
        

        if not comHubName is None:
            self.comHubName = comHubName
        else:            
            if self.comHubName is None:
                raise NameError("The comHubName argument must not be none "\
                                "if block.comHubName has been left to None.")                

        if not registerName is None:
            self.registerName = registerName
        else:            
            if self.registerName is None:
                raise NameError("The registerName argument must not be none "\
                                "if block.registerName has been left to None.")        
                                
        self.register = RegisterWrapper(self.registerName)

        if not stagingRoomName is None:
            self.stagingRoomName = stagingRoomName
        else:            
            if self.stagingRoomName is None:
                raise NameError("The stagingRoomName argument must not be none "\
                                "if block.stagingRoomName has been left to None.")         

        self.stagingRoom = StagingRoomWrapper(self.stagingRoomName)

        self.workerID = workerID
              

        if not self.register.isRegistered(self):
            self.register.registerBlock(self)

        if self.register.hasExecuted(self):                    
            self.print_(self.getSignature() + " has already been processed...")
            
            # Getting the output of the block as it may be necessary for
            # the execution of next blocks.
            outputDict = self.register.getOutputDict(self)
                                       
            for key in outputDict:
                self.__dict__[key] = outputDict[key]
        else:
            # Indicating that the module has ben entered.
            self.print_(self.getSignature() + " started...")
           
            self.register.registerExecutionStart(self)
            
            try:
                # Executing the module's code.
                if __PROFILING___:
                    # TODO: For now the profiling information is save into a file. 
                    # However, it should be tied to the block execution in the
                    # SQL database.
                    import cProfile
                    self.print_("starting profiling..."    )      
                    cProfile.runctx("self.execute()", globals(), locals(), filename=self.name + "-" + str(self.ID) + ".profile")
                else:
                    self.execute()
            except:
                # On error, print the error message and return such that the 
                # worker may continue working on other branch. The current 
                # branch is dead until it is restarted.                
                self.register.registerExecutionError(self)
                tracebackOutput = StringIO()
                traceback.print_exc(file=tracebackOutput)
                self.print_(tracebackOutput.getvalue())
                return
            
            # Indicating that the module's code execution is terminated.
            outputDict = dict((k,self.__dict__[k]) for k in self.outputs.keys()  if k in self.__dict__)
            self.register.registerExecutionCompletion(self, outputDict)
            self.print_(self.getSignature() + " completed..."    )                    
            
        # We want to promote in depth execution rather than lateral one, to avoid
        # generating too much processes. Thus, we favor that each block gives the 
        # priority to its first next block. For that, we do not release the available 
        # process but pass it directly to the first next block.

        # Running next blocks....
        # We use a copy of the nextBlockIDs field because the value of this
        # attribute is modified within the loop by the call to the
        # duplicateBranch(...) method.
        nextIDs = deepcopy(self.nextBlockIDs)
        for blockID in nextIDs:
            
            block = self.register.getBlock(blockID)    
                       
            nextInputs = None
            # Verify if the block needs input from this block.
            for inputKey in block.inputs:
                                      
                # If this input needs to come from the output of this block
                if isinstance(block.inputs[inputKey], fromPrevious):
                    
                    # verify if this block has a corresponding output.
                    if block.inputs[inputKey].outputKey in self.outputs:
                        
                        # Is the output atomic or parallel
                        if self.outputs[block.inputs[inputKey].outputKey] == "atomic":
                            if nextInputs is None : 
                                nextInputs = [{inputKey:self.__dict__[block.inputs[inputKey].outputKey]}]
                            else:
                                for nextInput in nextInputs:
                                    nextInput[inputKey] = self.__dict__[block.inputs[inputKey].outputKey]
                            
                        elif self.outputs[block.inputs[inputKey].outputKey] == "parallel":
                            
                            # Every nextInput needs to be duplicated for every items of
                            # the parallel output.
                            temp = []
                            for item in self.__dict__[block.inputs[inputKey].outputKey]:
                                if nextInputs is None : 
                                    temp.append({inputKey:item})
                                else:                                
                                    for nextInput in nextInputs:
                                        copyNI = deepcopy(nextInput)
                                        copyNI[inputKey] = item
                                        temp.append(copyNI)

                            nextInputs = temp
                        
                        else:
                            self.print_("Unknown output type.")                                   
                            raise NameError
                        
                    else: # there is no corresponding output
                        self.print_("No output/input correspondance: " + \
                                          block.inputs[inputKey].outputKey +\
                                         " is not in available outputs " + str(self.outputs))   
                        raise NameError 
             

            if nextInputs is None :
                self.stageBlock(block)                        
                   
            else :   
                for nextInput in nextInputs[:-1]:                            
                    newBlock = self.duplicateBranch(block, inputValues=nextInput, duplicationCorrespondance={})
                    newBlock.previousBlockIDs.remove(self.ID)                    
                    self.addNextBlock(newBlock)                      
                    self.stageBlock(newBlock)
                    self.register.updateBlock(self) 

                block.setInputFromPreviousOutput(nextInputs[-1])
                self.register.updateBlock(block)                                   
                self.stageBlock(block)
                                        


    def setInputFromPreviousOutput(self, fromPreviousOutput):
        for key in fromPreviousOutput:
            self.__dict__[key] = fromPreviousOutput[key]
            self.inputs[key].value   = fromPreviousOutput[key]            
            #self.print_(str(self.inputs[key]))

    def duplicateBranch(self, block, inputValues = None, 
                        idString = None, duplicationCorrespondance={}):
        """
         Duplicate a branch, starting from block and including all it nextBlocks.
         Inputs for the block of the root node are given as the inputValues parameter.
         Return the root of the new branch.
         
         When two blocks has a same next block, we need to duplicate this block
         only once. We therefore need to pass along a list of duplicated blocks
         in duplicationCorrespondance.   
        """            

        # Duplicate the block    
        newBlock = deepcopy(block) 
    
        # If it the the block corresponding to the root node
        if not inputValues is None:
            # Adjust its values of the inputs for the block. 
            newBlock.setInputFromPreviousOutput(inputValues) 
            idString = str(inputValues)

        # Create a new ID for this block, respecting the fact that it is a sibling
        # of block.                       
        newBlock.setForkID(idString)  
        duplicationCorrespondance[block.ID] = newBlock.ID   

        # Register the new block. It must be registered before calling 
        # duplicateBranch with newblock as its previousBlock parameter else
        # the previousBlock.addSiblingBlock(...) call fails.
        if not self.register.isRegistered(newBlock):
            self.register.registerBlock(newBlock)     

        # Ensure that there is no status left form previous incomplete 
        # processing.
        self.register.updateStatus(newBlock)

        # Duplicate the following blocks and update the nextBlockIDs accordingly.
        for nextBlockID in block.nextBlockIDs:
            
            if nextBlockID in duplicationCorrespondance:    
                # If this next block has already been duplicated, we just need to adjust
                # the relationships with its parent.
                nextBlock              = self.register.getBlock(duplicationCorrespondance[nextBlockID])    
                nextBlock.previousBlockIDs.remove(block.ID)     
                newBlock.nextBlockIDs.remove(nextBlockID)            
                newBlock.addNextBlock(nextBlock)    
                
            else:
                # We use the nextBlockIDs of block instead of this field of newblock
                # because this field in newblock is changed within the loop by the call
                # to the self.duplicateBranch(nextBlock, None, newblock) method.
                # At the onset of the loop, block.nextBlockIDs == newblock.nextBlockIDs
                nextBlock              = self.register.getBlock(nextBlockID)
                nextBlock.registerName = self.registerName         
                nextBlock.previousBlockIDs.remove(block.ID)     
                newBlock.nextBlockIDs.remove(nextBlockID)            
                nextBlock              = self.duplicateBranch(nextBlock, idString=idString, 
                                                              duplicationCorrespondance=duplicationCorrespondance)
                newBlock.addNextBlock(nextBlock)       
            
        return newBlock


    def stageBlock(self, block):

        if not self.register.isRegistered(block):
            self.register.registerBlock(block)
    
        readyToStage = True                        
        for blockID in block.previousBlockIDs :
            if not self.register.hasExecuted(ID = blockID):                        
                readyToStage = False                                     
                    
        if readyToStage:
            #self.print_("######## staging " +  str(block.name) + " " + str(block.ID))      
        
            # Stage only blocks that has not already been staged. This is
            # necessary for cases where more than one block has the same 
            # block in its next blocks.
        
            if not self.stagingRoom.hasBeenStaged(block):
                self.stagingRoom.stageBlock(block)   



    def __repr__(self):
        return self.serialize()


    def serialize(self):       
        serpentString = serpent.dumps(self, indent=False, set_literals=False)
        
        # Removing header because it conflicts with Pyros serializing
        serpentString = "\n".join(serpentString.split('\n')[1:])
        return serpentString



        
    @staticmethod
    def deserialize(serializedString):

        # Putting back the header removed at serialization
        header = serpent.dumps(1, indent=True, set_literals=False).split('\n')[0]

        AST = serpent.loads(header + "\n" + serializedString)
        
        return Block.fromASTtoObject(AST)
                            



    @staticmethod
    def fromASTtoObject(AST):
        className = AST["__class__"]            
        
        inputs = AST["inputs"]
        for inputKey in inputs :
            if isinstance(inputs[inputKey], dict) and '__class__' in inputs[inputKey] :  
                if inputs[inputKey]['__class__'] == "fromPrevious" :
                    if 'value' in inputs[inputKey]:
                        inputs[inputKey] = fromPrevious(inputs[inputKey]['outputKey'], inputs[inputKey]['value'])
                    else:
                        inputs[inputKey] = fromPrevious(inputs[inputKey]['outputKey'])                        
                    #inputs[inputKey] = "fromPrevious('" + inputs[inputKey]['outputKey'] + "')"                        
        
        
        keywords = {"inputs":inputs, "outputs":AST["outputs"],
                    "verbose":AST["verbose"], "ID":AST["ID"]}

        from PyBlockWork.blocks import *
        block = apply(locals()[className], [], keywords)  
        
        block.nextBlockIDs          = AST["nextBlockIDs"]
        block.previousBlockIDs      = AST["previousBlockIDs"]
        block.registerName          = AST["registerName"]
        block.name                  = AST["name"]
        block.depth                 = AST["depth"]
        
        for key in block.inputs:     
            if isinstance(block.inputs[key], fromPrevious):
                setattr(block, key, block.inputs[key].value)
        
        #if "fromPreviousOutput" in AST:
        #    block.fromPreviousOutput    = AST["fromPreviousOutput"]

        return block

        

"""
 Class used to identified the BWM input variables as comming form a previous
 block.
"""
class fromPrevious:
    def __init__(self, outputKey, value=None):
        self.outputKey = outputKey
        self.value     = value
    def __repr__(self):
        return "fromPrevious(outputKey='" + str(self.outputKey) + "', value=" + str(self.value) + ")"






