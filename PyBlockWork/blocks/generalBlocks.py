# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 13:42:13 2013

@author: oreichri
"""

import PyBlockWork
from glob import glob
import  os




"""
     Inputs  : "pattern" : String indicating the path and the file name format
                          (e.g., "C:\\DATA\\Julie\\*.sig")  
               "recursive" : (optionnal) If true, look for the pattern recursively
                             in the subdirectories located in the directory specified
                             in pattern. For example, if (recursive=true, 
                             pattern="C:\\DATA\\Julie\\*.sig") will find
                             "C:\\DATA\\Julie\\example.sig", but also
                             "C:\\DATA\\Julie\\dummy\\example.sig".
                          
     Outputs : "files"   : List of files according to the "pattern".
"""
class GlobBWM(PyBlockWork.Block):

    def execute(self): 

        if not hasattr(self, 'recursive'):
            self.recursive   =  False

        if self.recursive:
            import fnmatch     
            
            rootPath    = os.path.dirname(self.pattern)
            filePattern = os.path.basename(self.pattern)
            self.files   = []
            for root, dirnames, filenames in os.walk(rootPath):
              for filename in fnmatch.filter(filenames, filePattern):
                  self.files.append(os.path.join(root, filename)) 
        else:
            self.files = glob(self.pattern)


    def rollback(self):
        # No changes need to be canceled in this module.
        self.resetStatus() 
