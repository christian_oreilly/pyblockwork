# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 13:42:13 2013

@author: oreichri
"""


import PyBlockWork
import sys


from spyndle.propagation import XCSTEvaluator, SPFEvaluator
from spyndle.io import HarmonieReader, EDFReader, DatabaseMng
from spyndle.detector import SpindleDectectorRMS


"""
     Inputs  : "fileSIG"     : String indicating the full path to a .sig file.
               "channelList" : If specified, only channels in this list will be
                               recorded in the converted file.
                         
     Outputs : "files"   : String indicating the full path to a .bdf file.
"""
class SigConversionBWM(PyBlockWork.Block):

    def execute(self):   
        if self.verbose:        
            self.print_("Reading " + self.fileSIG + "...")
            
        readerSIG =  HarmonieReader(self.fileSIG)

        if not hasattr(self, 'type'):
            self.type = "EDF"
        if not hasattr(self, 'channelList'):
            self.channelList = None
        if not hasattr(self, 'fileName'):
            self.fileName = None
        if not hasattr(self, 'isSplitted'):
            self.isSplitted = True
        if not hasattr(self, 'annotationFileName'):
            self.annotationFileName = None
        if not hasattr(self, 'basePath'):
            self.basePath = None
        if not hasattr(self, 'annotationBasePath'):
            self.annotationBasePath = None

        if self.verbose:             
            self.print_("Saving a copy in the BDF format...")
            
        readerSIG.saveAsEDF(fileName=self.fileName, fileType = self.type, 
                    isSplitted=self.isSplitted, annotationFileName=self.annotationFileName,
                    channelList=self.channelList, basePath=self.basePath, 
                    annotationBasePath=self.annotationBasePath, verbose=self.verbose)        
        
        
        # Setting outputs
        self.file = self.fileSIG[:-3] + self.type.lower()


    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback SigConversionBWM"
        self.resetStatus() 



class SigBdfConversionBWM(SigConversionBWM):

    def execute(self):   
        self.type= "BDF"
        super(SigBdfConversionBWM, self).execute()
        self.fileBDF = self.file


    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        super(SigBdfConversionBWM, self).rollback()






"""
     Inputs  : "file"               : String indicating the full path to an EEG file.
               "readerClass"        : 
               "reformattingExpr"   :
                          
     Outputs : "fileReformatted"    : String indicating the full path to the reformated file.
"""
class ChannelReformattingBWM(PyBlockWork.Block):

    def execute(self):   
        if self.verbose:        
            self.print_("Reading " + self.file + "...")

        if hasattr(self, 'readerClass'):
            if not isinstance(self.readerClass, (str, unicode)) :
                raise TypeError("Expected type str or unicode, received type " + str(type(self.readerClass)) + ".")
            reader = apply(globals()[self.readerClass], [self.file])  
        else:
            reader =  EDFReader(self.file)              
            

        self.fileReformatted = self.file[:-4] + "_reformatted" + self.file[-4:]

        if self.verbose:             
            self.print_("Saving a reformated copy as " + self.fileReformatted  + "...")

        reader.reformatMontage(self.reformattingExpr, self.fileReformatted)



    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback ChannelReformattingBWM"
        self.resetStatus() 






                               
"""
     Inputs  : "file" : String indicating the full path to a .sig file.
               "includePatterns" : List of channel patterns to be listed. 
                                   (e.g., ['Fp1', 'Fp2', 'F7')
               "excludePatterns" : List of channel patterns to be excluded from
                                   the listing (e.g., ['Fp1', 'Fp2', 'F7').
               "readerClass" : (optional) Specify the reader class to be used.
                                EDFReader is used by default.
                          
     Outputs : "files"   : String indicating the full path to EEG file.
"""
class GetChannelListBWM(PyBlockWork.Block):

    def execute(self): 
                
        # A reader class can be spedicied as en input but the EDFReader is used
        # by default.

        if hasattr(self, 'readerClass'):
            if not isinstance(self.readerClass, (str, unicode)) :
                raise TypeError("Expected type str or unicode, received type " + str(type(self.readerClass)) + ".")
            reader = apply(globals()[self.readerClass], [self.file])  
        else:
            if (self.file.split(".")[-1]).lower() == "sig" :
                reader =  HarmonieReader(self.file)                    
            elif  (self.file.split(".")[-1]).lower() == "bdf" or \
                   (self.file.split(".")[-1]).lower() == "edf" :
                reader =  EDFReader(self.file)              
            else:
                raise ValueError("No reader class has been provided and the "\
                                 "program failed to recognized file extension "\
                                 "to guess the appropriate reader.")
            
        
        channelLabels = reader.getChannelLabels()

        OK = len(channelLabels)*[False]
        for ind, channel in enumerate(channelLabels):
            if len([include for include in self.includePatterns if include in channel]):
                OK[ind] = True
            if len([include for include in self.excludePatterns if include in channel]):
                OK[ind] = False                
                
        self.channelList = [channel for ind, channel in enumerate(channelLabels) if OK[ind]]
        rejectedChannels = [channel for ind, channel in enumerate(channelLabels) if not OK[ind]]

        if self.verbose:
            self.print_("Selected channels:" + str(self.channelList))
            self.print_("Rejected channels:" + str(rejectedChannels))
            sys.stdout.flush()



    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback GetChannelListBWM"
        self.resetStatus() 







"""
     Inputs  : "file" : String indicating the full path to a .sig file.
               "channelList" : List of channel on which we want to extract 
                                the spindles(e.g., ['Fp1-ref', 'Fp2-ref', 'F7-ref')
               "readerClass"  : (optional) Specify the reader class to be used.
                                EDFReader is used by default.
               "detectorClass": (optional) Specify the detector class to be used.
                                 SpindleDectectorRMS is used by default.
               "eventName"    : (optional) Specify the name of the spindle event.
                                 "SpindleRMS" is used by default.
               "detectionStages" : (optional) List of detection stages. 
                                   Default value : ["Sleep stage 2"]
               "detectionThreshold" : (optional) Detection threshold. 
               "computeSlopeFreq"   : (optional) Specify if the spindle frequency slope should be computed.
               "computeRMS"         : (optional) Specify if the spindle RMS amplitude should be computed.
               "computeFreq"        : (optional) Specify if the spindle average frequency  should be computed.
                          
     Outputs : "files"   : String indicating the full path to EEG file.
"""
class SpindleExtractionBWM(PyBlockWork.Block):

    def execute(self): 

        if self.verbose :        
            self.print_("Reading " + self.file + "...")
        
        # A reader class can be spedicied as en input but the EDFReader is used
        # by default.
        if hasattr(self, 'readerClass'):
            if not isinstance(self.readerClass, (str, unicode)) :
                raise TypeError("Expected type str or unicode, received type " + str(type(self.readerClass)) + ".")
            reader = apply(globals()[self.readerClass], [self.file])  
        else:
            if (self.file.split(".")[-1]).lower() == "sig" :
                reader =  HarmonieReader(self.file)                    
            elif  (self.file.split(".")[-1]).lower() == "bdf" or \
                   (self.file.split(".")[-1]).lower() == "edf" :
                reader =  EDFReader(self.file)              
            else:
                raise ValueError("No reader class has been provided and the "\
                                 "program failed to recognized file extension "\
                                 "to guess the appropriate reader.")          
                
            

        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"
                    
        # Remove any events that may be preexisting in the files and which 
        # has the same name as the events we want to add to avoid any mixing up.
        reader.events.remove([e for e in reader.events if e.name == self.eventName])
        
        if self.verbose :
            self.print_("Detecting spindles in " + self.file + "...")
            
        # A reader class can be spedicied as en input but the EDFReader is used
        # by default.            
        if hasattr(self, 'detectorClass'):
            if not isinstance(self.detectorClass, (str, unicode)):
                raise TypeError("Expected type str or unicode, received type " + str(type(self.detectorClass)) + ".")
            keywords = {"reader":reader, "usePickled":False}  
            detector = apply(globals()[self.detectorClass], [], keywords)  
        else:
            detector   =  SpindleDectectorRMS(reader, usePickled=False)            
        

        if not hasattr(self, 'detectionStages'):
            self.detectionStages = ["Sleep stage 2"]      
        
        detector.setDetectionStages(self.detectionStages)
        
        if hasattr(self, 'detectionThreshold'):       
            detector.threshold = self.detectionThreshold
            
        if hasattr(self, 'computeSlopeFreq'):       
            detector.computeSlopeFreq = self.computeSlopeFreq

        if hasattr(self, 'computeRMS'):       
            detector.computeRMS = self.computeRMS

        if hasattr(self, 'computeFreq'):       
            detector.computeFreq = self.computeFreq


        detector.detectSpindles(channelList=self.channelList)

        if self.verbose :        
            self.print_("Saving spindles...") 
            
            

        if not hasattr(self, 'dbPath'): 
            detector.saveSpindle(reader, self.eventName, "Spindle")
        else:
            
            with DatabaseMng(self.dbPath, shard=self.file) as dbm:
                try:
                    detector.saveSpindle(reader, self.eventName, "Spindle", dbSession=dbm.session)
                    dbm.session.commit()
                except:
                    dbm.session.rollback()
                    raise



    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback SpindleExtractionBWM"
        self.resetStatus() 

            
            
            
        






    
"""
     Inputs  : "file" : String indicating the full path to a .sig file.
               "channelList" : List of channel on which we want to extract 
                                the spindles(e.g., ['Fp1-ref', 'Fp2-ref', 'F7-ref')
               "resSubDir"    : Subdirectory where to save the results. If the directory
                                does not exist, it is created. (e.g., "Results/")
               "resFilesPrefix": Prefixe of the result files if results is to be
                                 stored in text files (e.g., "spindleDelays_offset").
               "dbPath"     : 
               "offset"       : Time offset of the comparison in seconds (e.g., 5.0).
               "eventName"    : (optional) Specify the name of the spindle event.
                                 "SpindleRMS" is used by default.
                          
     Outputs : filesOut   : String indicating the full path to EEG file.
               pathOut    :
               channelList :
               night      :
"""    
class SpindlePropagationBWM(PyBlockWork.Block):

    def execute(self):         
        
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   


        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 
            
        try:
                    
            evaluator = XCSTEvaluator(self.eventName, self.verbose)        
            evaluator.createEEGReader(self.file, EDFReader)
            evaluator.prepareDatabase(dbSession=None, dbPath=self.dbPath, shard=self.file)   
            evaluator.compute(delta =0.5, offset=self.offset, 
                              channelList=self.channelList,  eventProperties = [])
                                    
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  




    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback SpindlePropagationBWM"
        self.resetStatus() 




    
"""                

"""    

class ComputeSPF_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

        if not hasattr(self, 'offsetSync'): 
            self.offsetSync  = 0.0
            
        if not hasattr(self, 'offsetAsync'): 
            self.offsetAsync  = 5.0
            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            
            
        if not hasattr(self, 'addBehavior'): 
            self.addBehavior  = 'raise'
            
        if not hasattr(self, 'alpha'): 
            self.alpha  = 10.0

            
        try:
            evaluator = SPFEvaluator(self.night, self.eventName, 
                                     dbName = self.dbPath, shard=self.night, verbose = self.verbose)        
            evaluator.computeSPF(self.channelList, self.addBehavior, self.alpha)    
                 
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  




    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback ComputeSPF_BWM"
        self.resetStatus() 






















class PropagationRejection_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            
            
        if not hasattr(self, 'addBehavior'): 
            self.addBehavior  = 'raise'
            
        if not hasattr(self, 'alpha'): 
            self.alpha  = 10.0

            
        try:
            evaluator = SPFEvaluator(self.night, self.eventName,
                                     dbName = self.dbPath, shard=self.night, verbose = self.verbose)        
            evaluator.addBehavior = self.addBehavior
            evaluator.propagationRejection(self.channelList, self.alpha)             
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  




    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback PropagationRejection_BWM"
        self.resetStatus() 






class NegativeDelayCorrection_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            
            
        try:
            evaluator = SPFEvaluator(self.night, self.eventName, 
                                     dbName = self.dbPath, shard=self.night, verbose = self.verbose)        
            evaluator.negativeDelayCorrection()           
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  


    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback NegativeDelayCorrection_BWM"
        self.resetStatus() 







class IdentifySPF_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

        if not hasattr(self, 'addBehavior'): 
            self.addBehavior  = 'raise'
            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            

        evaluator = SPFEvaluator(self.night, self.eventName, 
                                 dbName = self.dbPath, shard=self.night, verbose = self.verbose)                
        try:    
            evaluator.addBehavior = self.addBehavior
            evaluator.identifySPF()           
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:      
            evaluator.dbSession.close()  




    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback IdentifySPF_BWM"
        self.resetStatus() 





class RemoveBidirectionnalDuplicates_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            
            
        try:
            evaluator = SPFEvaluator(self.night, self.eventName,
                                     dbName = self.dbPath, shard=self.night, verbose = self.verbose)        
            evaluator.removeBidirectionnalDuplicates()           
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  



    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback RemoveBidirectionnalDuplicates_BWM"
        self.resetStatus() 
    
    






class ComputeAveragePropagation_BWM(PyBlockWork.Block):
       

    def execute(self):         
        
        if not hasattr(self, 'night'):
            raise ValueError("No value given for the night parameter.")

        if not hasattr(self, 'channelList'): 
            raise ValueError("No value given for the channelList parameter.") 
            
            
        if not hasattr(self, 'eventName'):
            self.eventName = "SpindleRMS"   

        if not hasattr(self, 'dbPath'): 
            raise AttributeError("A database path must be provided.")
            
        if not hasattr(self, 'verbose'): 
            self.verbose = False 

            
        if not hasattr(self, 'dbPath'): 
            self.dbPath  = "sqlite:///:memory:"
            
            
        if not hasattr(self, 'deltaWindow'): 
            self.deltaWindow  = 0.5
            
            
        if not hasattr(self, 'alphaSD'): 
            self.alphaSD  = 0.2
            
            
        if not hasattr(self, 'minNbValid'): 
            self.minNbValid  = 40

            
            
        try:
            evaluator = SPFEvaluator(self.night, self.eventName,
                                     dbName = self.dbPath, shard=self.night, verbose = self.verbose)        
            evaluator.computeAveragePropagation(deltaWindow = self.deltaWindow, 
                                                alphaSD = self.alphaSD, minNbValid=self.minNbValid)           
            evaluator.dbSession.commit()
        except:
            evaluator.dbSession.rollback()
            raise
        finally:
            evaluator.dbSession.close()  



    def rollback(self):
        #TODO: This method has to implemented the code to cancel out 
        #any persistent change made during the block execution.
        print "Rollback ComputeAveragePropagation_BWM"
        self.resetStatus() 
    
