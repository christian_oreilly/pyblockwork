# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 00:12:09 2013

@author: Christian
"""


from datetime import datetime
from Queue import Queue, Empty

class Message:
    def __init__(self, msgString):
        self.msgString   = msgString
        self.msgDateTime = datetime.now()
        
    def __repr__(self):
        return self.msgDateTime.strftime("%H:%M:%S") + " >> " + self.msgString                



# TODO: Use the new message classe to implement messages rather than simple string
# such that other properties (e.g. message time) can be accessed.

class CommunicationHub:
    """
     The communication hub is used to aggregate the messages from the various
     execution blocks and provide them on demand to any client. This object is used
     as a mediator between the message displaying process and the block process to
     avoid that pause of executing blocks due to access to shared memory by 
     diplayer pooling. 
    """   

    def __init__(self):
        
        # Keys are PYRO URIs, values are BlockQueue objects
        self.__queue        = {}
        self.blockMessages  = {}
        self.blockListener  = {}
        
        
    def addWorker(self, ID):
        self.__queue[ID] = WorkerQueue()  
        
        
    def addBlockListener(self, blockID, listenerObject):
        if blockID in self.blockListener:
            self.blockListener[blockID].append(listenerObject)
        else:
            self.blockListener[blockID]  = [listenerObject]
                
    def removeBlockListener(self, blockID, listenerObject):
        if blockID in self.blockListener:

            self.blockListener[blockID].remove(listenerObject)            
                
                
        
    def writeMessage(self, ID, message, blockID = None):
        self.__queue[ID].writeMessage(Message(message))
        
        if not blockID is None:
            if blockID in self.blockMessages:
                self.blockMessages[blockID] +=  str(Message(message)) + "\n"
            else:
                self.blockMessages[blockID]  =  str(Message(message)) + "\n"
                
            if blockID in self.blockListener:
                for listener in self.blockListener[blockID]:
                    listener.blockMessageEmitted(message) 
                                    
                
                
                
                
    """
    Return the recorded message for worker ID, removing it from the communication
    hub message queue for the corresponding worker.
    """
    def readMessage(self, ID):
        message = self.__queue[ID].readMessage()
        if not self.__queue[ID].isRunning:
            del self.__queue[ID]
        return str(message)
                        

    def empty(self, ID):
        return self.__queue[ID].empty()

    def full(self, ID):
        return self.__queue[ID].full()

                
                
    """
    Read block messages.
    """
    def readBlockMessages(self, blockID):
        if blockID in self.blockMessages:  
            return self.blockMessages[blockID]
        else:
            return ""
        
        
        
        
        

    def getReaderIDs(self):
        return self.__queue.keys()   
        
        
    def terminateWorker(self, ID):    
        self.__queue[ID].isRunning = False     


    def test(self):
        pass






class WorkerQueue:
    
    def __init__(self):
        
        # A BlockPipe should be deleted from the CommunicationHub as soon as it 
        # is not running and  its has an empty msgQueue
        # is empty.
        self.isRunning     = True
        self.msgQueue      = Queue()
        
    def writeMessage(self, message):
        self.msgQueue.put(message)
                
    def readMessage(self):
        try:
            return self.msgQueue.get(False)
        except Empty:
            return ""
        
    def empty(self):
        return self.msgQueue.empty()
                
    def full(self):
        return self.msgQueue.full()
        
     