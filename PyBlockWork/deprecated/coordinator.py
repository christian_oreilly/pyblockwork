# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""


from cPickle import dump, load
import os
from multiprocessing import Semaphore, Queue
import Pyro4
from PyBlockWork import Block
import warnings

warnings.warn("This module has been deprecated.", DeprecationWarning)    


class RegisterItem:
    def __init__(self):
        self.hasStarted  = False
        self.hasExecuted = False


class Register:

    def __init__(self, pickleFileName = "C:/temp.bin"):
        
        # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        self.registerSempaphore = Semaphore(1)   

        self.pickleFileName = pickleFileName
              
        if os.path.isfile(pickleFileName) : 
            with open(pickleFileName, "rb") as pickleFile :
                self.register = load(pickleFile)
        else:
            self.register = {}




    def registerExecutionStart(self, blockSignature):

        with self.registerSempaphore :  
            if not blockSignature in self.register:
                self.register[blockSignature] = RegisterItem()                   
            self.register[blockSignature].hasStarted = True
                        
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        
        
        
        
    def registerExecutionCompletion(self, blockSignature, outputDict):

        with self.registerSempaphore :   
            if not blockSignature in self.register:
                self.register[blockSignature] = RegisterItem()                   
            self.register[blockSignature].hasExecuted = True
            self.register[blockSignature].outputDict  = outputDict
                
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        


    def hasExecuted(self, blockSignature):
 
        with self.registerSempaphore :    
                      
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].hasExecuted
            else:
                retVal = False
        
        return retVal



    def getOutputDict(self, blockSignature):
        with self.registerSempaphore :         
                 
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].outputDict
            else:
                retVal = None
                
        return retVal


    def test(self):
        pass







class StagingRoomWrapper:
    """
     Class used wrapping the StagingRoom class. The goal of this object if class
     is to provide a local interface to remote objects.
     
     This class allows to simplify the management of blocks. To simplify the
     serialization, block objects don't contain references to other blocks but
     rather identification strings associated with other blocks. Also, the staging 
     room do not store blocks but string representations of blocks. However, 
     getting string representation of blocks is not practical because we have 
     then to reconstruct the block from the reprensentation. These complications
     are managed by this wrapper such that we can interact with the staging room
     more convivialy. 
    """
    
    def __init__(self, stagingRoomName):
        
        self.stagingRoomName = stagingRoomName
        
    
    def getblock(self, blockID):
        blockString = self.stagingRoomExec(StagingRoom.getBlockString, blockID)
        return Block.deserialize(blockString)        
    
    def registerBlock(self, block):
        self.stagingRoomExec(StagingRoom.registerBlockString, block.ID, block.serialize())    
    
    def updateBlock(self, block):
        self.stagingRoomExec(StagingRoom.updateBlockString, block.ID, block.serialize())            
    
    def stageBlock(self, block):
        self.stagingRoomExec(StagingRoom.addReadyBlock, block.ID)     
        
    
    
    def stagingRoomExec(self, func, *args, **kargs):
        if self.stagingRoomName is None:
            raise Exception("The staging room name is set to None. Connection to the staging room not possible.")
        
        try:
            with Pyro4.Proxy("PYRONAME:" + self.stagingRoomName) as stagingRoom:   
                
                # __func__ is used to get around the fact that stagingRoom is not of type
                # StagingRoom but Proxy, which causes problems in python 
                # 2.6+ for unbound methods.
                #return func.__func__(stagingRoom, *args, **kargs)  

                if func == StagingRoom.registerBlockString:
                    stagingRoom.registerBlockString(args[0], args[1])
                elif func == StagingRoom.getBlockString:
                    return stagingRoom.getBlockString(args[0])
                   
        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback()) 
            raise
        

        
        
        

"""
 Class used to identified the BWM input variables as comming form a previous
 block.
"""
class StagingRoom:
    def __init__(self):
                
        # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        #self.registerSempaphore = Semaphore(1)   

        # Structure of the schema, such that the scheduler knows enough information
        # to provide it when asked (for example, by an object wanting to draw
        # the schema)
        #self.schemaStructure = None

        self.waitingBlockQueue = Queue()
        
        
        self.blockStrings = {}
        
        
    def registerBlockString(self, ID, blockString):
        if ID in self.blockStrings:
            raise ValueError("The block with ID " + str(ID) + 
                             " has already been registered. To update its " + 
                             "blockString, use the updateBlockString method.")
                             
        self.blockStrings[ID] = blockString        
        
        
    def updateBlockString(self, ID, blockString):
        if not ID in self.blockStrings:
            raise ValueError("The block with ID " + str(ID) + 
                             " is not registered. To register it, " + 
                             "use the registerBlockString method.")
                             
        self.blockStrings[ID] = blockString



    def getBlockString(self, ID):
        if not ID in self.blockStrings:
            raise ValueError("The ID " + ID + " has not been registered.")
        return self.blockStrings[ID]



    def addReadyBlock(self, block):
        self.waitingBlockQueue.put(block)
              
    def getReadyBlock(self):
        return self.waitingBlockQueue.get()
              
              
    def getReadyBlockString(self):     
        return self.getBlockString(self.getReadyBlock())



    def test(self):
        pass


      
      
      
    """
    @property
    def dbPath(self):
        return self.__dbPath

    @dbPath.setter
    def dbPath(self, value): 
        self.__dbPath = value
    

    #def setSchemaStructure(self, schemaStructure):
    #    self.schemaStructure = schemaStructure
        
    #def getSchemaStructure(self):
    #    return self.schemaStructure

    def getNbMaxProcesses(self):
        return self.nbMaxProcesses 

    def changeNbMaxProcesses(self, nbMaxProcesses):    
        diffNbProcesses = nbMaxProcesses - self.nbMaxProcesses

        while diffNbProcesses > 0:
            self.processesSemaphore.release()             
            diffNbProcesses -= 1;
            self.nbMaxProcesses += 1
            
        while diffNbProcesses < 0:
            print "Trying to remove", diffNbProcesses, "items from the semaphore...",
            print "It may take some time for them to get avaiable... waiting..."
            self.processesSemaphore.release()             
            diffNbProcesses += 1;
            self.nbMaxProcesses -= 1
            


        
    def queryRegisterSemaphore(self):
          # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        available = self.registerSempaphore.acquire(False)  
        if available:
             self.registerSempaphore.release()  
        return available



    def queryProcessesSemaphore(self):
          # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        nbAvailable = 0        
        
        while(self.processesSemaphore.acquire(False)):
            nbAvailable += 1
            
        for i in range(nbAvailable):
             self.processesSemaphore.release()  
             
        return nbAvailable

    def acquireProcess(self):#, acquiringProcess):
        #self.processesList.append(acquiringProcess)
        self.processesSemaphore.acquire()
        
    def releaseProcess(self):
        self.processesSemaphore.release()
        
    #def transferProcess(self, releasingProcess, acquiringProcess):
    #    self.processesList.append(acquiringProcess)



    def test(self):
        pass

    def registerExecutionStart(self, blockSignature):

        with self.registerSempaphore :  
            if not blockSignature in self.register:
                self.register[blockSignature] = RegisterItem()                   
            self.register[blockSignature].hasStarted = True
                        
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
            f.close()
        
            
        print blockSignature, "started..."
        sys.stdout.flush()                
        
        
        
    def registerExecutionCompletion(self, blockSignature, outputDict):

        with self.registerSempaphore :   
            if not blockSignature in self.register:
                self.register[blockSignature] = RegisterItem()                   
            self.register[blockSignature].hasExecuted = True
            self.register[blockSignature].outputDict  = outputDict
                
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
            f.close()
        
        print blockSignature, "completed..."
        sys.stdout.flush() 


    def hasExecuted(self, blockSignature):
 
        with self.registerSempaphore :    
                      
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].hasExecuted
            else:
                retVal = False
        
        

        return retVal

    def getOutputDict(self, blockSignature):
        with self.registerSempaphore :         
                 
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].outputDict
            else:
                retVal = None



        return retVal


    def __del__(self):
        pass
        #self.cleanExit()


    """





