# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""


from cPickle import dump, load
import os
from multiprocessing import Semaphore

import warnings


warnings.warn("This module has been deprecated. Use the Register and the StagingRoom modules.", DeprecationWarning)    


class SchedulerItem:
    def __init__(self):
        self.hasStarted  = False
        self.hasExecuted = False

"""
 Class used to identified the BWM input variables as comming form a previous
 block.
"""
class Scheduler:
    def __init__(self, pickleFileName = "C:/temp.bin", nbMaxProcesses=10):
        
        self.nbMaxProcesses     = nbMaxProcesses
        
        # Make nbMaxProcesses available for multithreading
        self.processesSemaphore = Semaphore(nbMaxProcesses) 
        
        # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        self.registerSempaphore = Semaphore(1)   

        # Structure of the schema, such that the scheduler knows enough information
        # to provide it when asked (for example, by an object wanting to draw
        # the schema)
        self.schemaStructure = None
        
        self.NbSregister  = nbMaxProcesses
        self.NbSprocesses = 1

        self.__dbPath = ""

        self.pickleFileName = pickleFileName

              
        if os.path.isfile(pickleFileName) : 
            with open(pickleFileName, "rb") as pickleFile :
                self.register = load(pickleFile)
        else:
            self.register = {}
            
       
    @property
    def dbPath(self):
        return self.__dbPath

    @dbPath.setter
    def dbPath(self, value): 
        self.__dbPath = value
    

    #def setSchemaStructure(self, schemaStructure):
    #    self.schemaStructure = schemaStructure
        
    #def getSchemaStructure(self):
    #    return self.schemaStructure

    def getNbMaxProcesses(self):
        return self.nbMaxProcesses 

    def changeNbMaxProcesses(self, nbMaxProcesses):    
        diffNbProcesses = nbMaxProcesses - self.nbMaxProcesses

        while diffNbProcesses > 0:
            self.processesSemaphore.release()             
            diffNbProcesses -= 1;
            self.nbMaxProcesses += 1
            
        while diffNbProcesses < 0:
            print "Trying to remove", diffNbProcesses, "items from the semaphore...",
            print "It may take some time for them to get avaiable... waiting..."
            self.processesSemaphore.release()             
            diffNbProcesses += 1;
            self.nbMaxProcesses -= 1
            


        
    def queryRegisterSemaphore(self):
          # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        available = self.registerSempaphore.acquire(False)  
        if available:
             self.registerSempaphore.release()  
        return available



    def queryProcessesSemaphore(self):
          # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        nbAvailable = 0        
        
        while(self.processesSemaphore.acquire(False)):
            nbAvailable += 1
            
        for i in range(nbAvailable):
             self.processesSemaphore.release()  
             
        return nbAvailable

    def acquireProcess(self):#, acquiringProcess):
        #self.processesList.append(acquiringProcess)
        self.processesSemaphore.acquire()
        
    def releaseProcess(self):
        self.processesSemaphore.release()
        
    #def transferProcess(self, releasingProcess, acquiringProcess):
    #    self.processesList.append(acquiringProcess)



    def test(self):
        pass

    def registerExecutionStart(self, blockSignature):

        with self.registerSempaphore :  
            if not blockSignature in self.register:
                self.register[blockSignature] = SchedulerItem()                   
            self.register[blockSignature].hasStarted = True
                        
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
            f.close()
        
            
        print blockSignature, "started..."
        sys.stdout.flush()                
        
        
        
    def registerExecutionCompletion(self, blockSignature, outputDict):

        with self.registerSempaphore :   
            if not blockSignature in self.register:
                self.register[blockSignature] = SchedulerItem()                   
            self.register[blockSignature].hasExecuted = True
            self.register[blockSignature].outputDict  = outputDict
                
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
            f.close()
        
        print blockSignature, "completed..."
        sys.stdout.flush() 


    def hasExecuted(self, blockSignature):
 
        with self.registerSempaphore :    
                      
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].hasExecuted
            else:
                retVal = False
        
        

        return retVal

    def getOutputDict(self, blockSignature):
        with self.registerSempaphore :         
                 
            if blockSignature in self.register:        
                retVal = self.register[blockSignature].outputDict
            else:
                retVal = None



        return retVal


    def __del__(self):
        pass
        #self.cleanExit()





###############################################################################
# Processing schema definition
###############################################################################


import Pyro4
import sys


if __name__ == '__main__': 

    sys.excepthook = Pyro4.util.excepthook
    
    scheduler = Scheduler("C:\\DATA\\Julie\\schedulerRegister.bin", 15)

    #Pyro4.Daemon.serveSimple({scheduler:"scheduler"}, ns=True)
           
    Pyro4.config.HMAC_KEY='sdfsdf7s98d7f98sd79f!?fghJLIHJKH'    
    Pyro4.config.THREADPOOL_MAXTHREADS = 500
     
    deamon = Pyro4.Daemon()
    ns = Pyro4.locateNS()
    ns.register("scheduler", deamon.register(scheduler))

    deamon.requestLoop()

