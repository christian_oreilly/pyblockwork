# -*- coding: utf-8 -*-
"""
Created on Tue Jun 04 22:59:38 2013

@author: oreichri
"""



from Pyro4 import threadutil, naming
import Pyro4
import sys
import time
from threading import Thread
from PyBlockWork import Scheduler, CommunicationHub, StagingRoom, Register
import warnings


warnings.warn("This module has been deprecated. Use the pyroServer module.", DeprecationWarning)    

class PyroServer(threadutil.Thread):
    def __init__(self, host, isDeamon, port=0, enableBroadcast=True, 
                 bchost=None, bcport=None, unixsocket=None, nathost=None, natport=None):

        super(PyroServer,self).__init__()
        
        """
         Start a name server
        """
        self.setDaemon(isDeamon)
        self.host=host
        self.started=threadutil.Event()
        self.unixsocket = unixsocket
        self.port = port
        self.enableBroadcast = enableBroadcast 
        self.bchost = bchost
        self.bcport = bcport
        self.nathost = nathost
        self.natport = natport       

        # This code is taken from Pyro4.naming.startNSloop
        self.ns_daemon = naming.NameServerDaemon(self.host, self.port, self.unixsocket, 
                                                 nathost=self.nathost, natport=self.natport)
        self.uri    = self.ns_daemon.uriFor(self.ns_daemon.nameserver)
        internalUri = self.ns_daemon.uriFor(self.ns_daemon.nameserver, nat=False)
        self.bcserver=None
        self.ns = self.ns_daemon.nameserver        
        
        if self.unixsocket:
            hostip = "Unix domain socket"
        else:
            hostip = self.ns_daemon.sock.getsockname()[0]
            if hostip.startswith("127."):
                enableBroadcast=False
            if enableBroadcast:
                # Make sure to pass the internal uri to the broadcast responder.
                # It is almost always useless to let it return the external uri,
                # because external systems won't be able to talk to this thing anyway.
                bcserver=naming.BroadcastServer(internalUri, self.bchost, self.bcport)
                bcserver.runInThread()


        """
         Start a pyro object server.
        """      
        self.pyroObjectServer = Pyro4.Daemon(self.host)                 # make a Pyro daemon
            
        thread = Thread(target = self.pyroObjectServer.requestLoop)
        thread.setDaemon(1)
        thread.start()
        time.sleep(1)



    def registerObject(self, obj, pyroName):        
        uri=self.pyroObjectServer.register(obj)   # register the greeting object as a Pyro object
        self.ns.register(pyroName, uri)  # register the object with a name in the name server
    



    def run(self):
        try:
            self.ns_daemon.requestLoop()
        finally:
            self.ns_daemon.close()
            if self.bcserver is not None:
                self.bcserver.close()

        
    def startNS(self):
        self.start()
        
    def stopNS(self):
        self.ns_daemon.shutdown()
        if self.bcserver is not None:
            self.bcserver.shutdown()
        
        


# Helper function
def startNameServer(host, isDeamon):
    ns=PyroServer(host, isDeamon)
    ns.startNS()
    return ns




###############################################################################
# DEPRECATED METHODS TO BE REMOVE IN v 0.3.0

def startScheduler(register, nbProcesses, ns, schedulerName="scheduler", 
                   databaseMng="sqlite", databasePath=""):


    warnings.warn("This function has been deprecated. Use the"\
                  " PyroServer class.", DeprecationWarning)    

    if databasePath == "":
        databasePath = "c:/DATA/" + schedulerName + ".db"

    scheduler = Scheduler(register, nbProcesses)
    scheduler.dbPath = str(databaseMng + ":///" + databasePath)


    daemon=Pyro4.Daemon()                 # make a Pyro daemon
    uri=daemon.register(scheduler)   # register the greeting object as a Pyro object
    ns.register(schedulerName, uri)  # register the object with a name in the name server

    thread = Thread(target = daemon.requestLoop)
    thread.setDaemon(1)
    thread.start()
    time.sleep(1)



def registerPyros(pyroInfos, ns, host=None, port=0):

    warnings.warn("This function has been deprecated. Register objects using the method"\
                  " PyroServer.registerObject()", DeprecationWarning)

    if not isinstance(pyroInfos, dict):
        raise TypeError("pyroInfos must be a dict object.")

    daemon=Pyro4.Daemon(host, port)                 # make a Pyro daemon
        
    classes = {"stagingRoomName": StagingRoom, 
               "comHubName"     : CommunicationHub, 
               "registerName"   : Register}
    for key in classes:                                                 
        if key in pyroInfos:
            
            Class    = classes[key]
            if isinstance(pyroInfos[key], (list, tuple)):
                pyroName = pyroInfos[key][0]
                arg      = pyroInfos[key][1:]
            else:
                pyroName = pyroInfos[key]
                arg      = ()

            print "PyroRegister:", str(Class), str(arg)
            obj = Class(*arg)


    
            uri=daemon.register(obj)   # register the greeting object as a Pyro object
            ns.register(pyroName, uri)  # register the object with a name in the name server

    thread = Thread(target = daemon.requestLoop)
    thread.setDaemon(1)
    thread.start()
    time.sleep(1)











def startServers(register, nbProcesses, startNS=None, schedulerName="scheduler", 
                         NSisDeamon=False, databaseMng="sqlite", databasePath="",
                         HMAC_KEY=""):

    warnings.warn("This function has been deprecated. Use the"\
                  " PyroServer class.", DeprecationWarning)


    # Configuring Pyro
    sys.excepthook = Pyro4.util.excepthook    
    Pyro4.config.HMAC_KEY=HMAC_KEY
    Pyro4.config.THREADPOOL_MAXTHREADS = 500



    # Starting the name server
    if startNS :
        print 'Waiting for the Pyro nameserver to start...'
        startNameServer("localhost", NSisDeamon)  
        time.sleep(2.5)
        ns = Pyro4.locateNS()
    elif startNS is None:
        try:
            ns = Pyro4.locateNS()
            print "A name server has been located..."
        except : #errors.PyroError:
            print 'No name server located. Trying to start one.'
            print 'Waiting for the Pyro nameserver to start...'
            startNameServer("localhost", NSisDeamon)  
            time.sleep(2.5)       
            ns = Pyro4.locateNS()           

    # Starting the scheduler
    print "Waiting for the scheduler named " + schedulerName + " to start..."
    startScheduler(register, nbProcesses, ns, schedulerName, 
                   databaseMng, databasePath)
        
