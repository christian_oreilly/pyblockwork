# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""


from abc import ABCMeta, abstractmethod 
from copy import deepcopy
import sys
import Pyro4
from PyBlockWork import Register



class BWModule():
    __metaclass__ = ABCMeta
    
    """
     inputs  : Is a dictionnary of variableLabels:variableValues. Big objects
               should be pickled and only the file name should be passed because
               input objects are used in the object signature.
     outputs : A dictionnaly of output variableLabels:outputProperties:
               "parallel": for an object that which all elements can be proceed
                           independently.
               "atomic"  : for an object that which...
    """
    def __init__(self, inputs, outputs, verbose = True):

        for inputKey in inputs:
            self.__dict__[inputKey] = inputs[inputKey]

        self.verbose  = verbose
        self.outputs  = outputs

        # We keep this as it is used in buliding the signature of the block.
        self.inputs = inputs

        self.nextBlocks = []
        
        # Tells that this block is an entry point (the first block of the schema).
        # This is set to false before any call to the start() method of a subsequent
        # block, such that only the block started by the user has this value as True.
        self.isEntryPoint = True
        
        # True if these is a process is avaiable. It should always be set to
        # True by the preceeding blocks, passing its available processing slot,
        # except for the first block of the schema which need to request it
        # directly from the manager.
        self.processAvailable = False        

        sys.excepthook = Pyro4.util.excepthook    
        self.registerName = "scheduler"
        self.scheduler = None          


    def setSchedulerName(self, name):
        self.schedulerName = name

    def addNextBlock(self, block):
        self.nextBlocks.append(block)


    """
     Should be implemented with the code to execute. In this code, input and
     output variables must be preceded by "self."
    """
    @abstractmethod
    def execute(self): 
        raise "Error: Pure virtual function"

    """
     Return the signature of the BWM.
    """
    def getSignature(self):
        return str(type(self)) + ":" + str(self.inputs) + ":" + str(self.outputs)

    
    def setEntryPoint(self, isEntryPoint):
        self.isEntryPoint = isEntryPoint



    def getSchemaStructure(self):

        return {"name":self.__class__.__name__,
                "inputs":self.inputs,
                "outputs":self.outputs,
                "nexts":[b.getSchemaStructure() for b in self.nextBlocks] }


    def print_(self, string):
        try:
            with Pyro4.Proxy("PYRONAME:" + self.pyroInfos["comHubName"]) as comHub:   
                comHub.writeMessage(self.ID, string)  
        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback())     




    def registerExec(self, func, *args, **kargs):
        try:
            with Pyro4.Proxy("PYRONAME:" + self.pyroInfos["registerName"]) as register:   
                return func(register, *args, **kargs)  
        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback())            
        
        
        
    
    def stageBlock(self, block):
        try:
            with Pyro4.Proxy("PYRONAME:" + self.pyroInfos["stagingRoomName"]) as stagingRoom:   
                stagingRoom.addBlock(block)
        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback())            
        
        
        
      

    """
     The function used to start the processing of the block. Normally, it should
     by a BlockWorker object.
    """
    def run(self, ID, pyroInfos): 
        
        self.pyroInfos = pyroInfos        
        
        if not isinstance(pyroInfos, dict):
            raise TypeError("pyroInfos must be a dict object.")
            
        for key in ["stagingRoomName", "comHubName", "registerName", "HMAC_KEY"]:                                                 
            if not key in pyroInfos:
                raise NameError("pyrInfos must contain the key " + key + ".")        
        
        self.ID = ID
        Pyro4.config.HMAC_KEY = pyroInfos["HMAC_KEY"]           
        
        #print "Nb Next blocks:", len(self.nextBlocks)
        #sys.stdout.flush()            
        

        # Some outputes are coming from the previous block.    
        if "fromPreviousOutput" in self.__dict__ :
            print "fromPreviousOutput: ", self.fromPreviousOutput
            for key in self.fromPreviousOutput:
                self.__dict__[key] = self.fromPreviousOutput[key]
                self.inputs[key]   = self.fromPreviousOutput[key]        

        
        try:
            Pyro4.locateNS()
        except Pyro4.errors.PyroError, e:
            print 'Pyro error message: ' + str(e)
            raise
        except:
            raise
        
        #== self.scheduler = Pyro4.Proxy("PYRONAME:" + self.schedulerName)                     
        
              
 
        #try:
        
        # If this is true, this block is the entry point of the schema. We must
        # therefore tell the Scheduler about the schema structure for it to be
        # able to gives information necessary to display the schema.
        #            
        # PROBLEM: Setting the schema structure this way make the scheduler
        # unserializable because of too deep structure caused by chaining
        # of blocks.
        #if self.isEntryPoint :        
        #    self.scheduler.setSchemaStructure(self.getSchemaStructure())
                           
                   
                   
        # If self.processAvailable is true, the available slot has been passed
        # from a previous block so there is no need to ask it to the scheduler.
        # Else, it must be gathered from the scheduler.
        #== if not self.processAvailable :         
        #==     self.scheduler.acquireProcess()    
        #==     self.processAvailable = True
            
        if self.registerExec(Register.hasExecuted, self.getSignature()):                    
            self.print_(self.getSignature() + " has already been processed...")
            
            # Getting the output of the block as it may be necessary for
            # the execution of next blocks.
            outputDict = self.registerExec(Register.getOutputDict, self.getSignature())
                                       
            for key in outputDict:
                self.__dict__[key] = outputDict[key]
        else:
            # Indicating that the module has ben entered.
            self.print_(self.getSignature() + " started...")
           
            self.registerExec(Register.registerExecutionStart, self.getSignature())
                
            # Executing the module's code.
            self.execute()
            
            # Indicating that the module's code execution is terminated.
            outputDict = dict((k,self.__dict__[k]) for k in self.outputs.keys()  if k in self.__dict__)
            self.registerExec(Register.registerExecutionCompletion, self.getSignature(), outputDict)
            self.print_(self.getSignature() + " completed..."    )                    
            
        # We want to promote in depth execution rather than lateral one, to avoid
        # generating too much processes. Thus, we favor that each block gives the 
        # priority to its first next block. For that, we do not release the available 
        # process but pass it directly to the first next block.
                   

        # Running next blocks....
        for block in self.nextBlocks:
            nextInputs = None
            # Verify if the block needs input from this block.
            for inputKey in block.inputs:
                
                # If this input needs to come from the output of this block
                if isinstance(block.__dict__[inputKey], fromPrevious):
                    
                    # verify if this block has a corresponding output.
                    if block.__dict__[inputKey].outputKey in self.outputs:
                        
                        # Is the output atomic or parallel
                        if self.outputs[block.__dict__[inputKey].outputKey] == "atomic":
                            if nextInputs is None : 
                                nextInputs = [{inputKey:self.__dict__[block.__dict__[inputKey].outputKey]}]
                            else:
                                for nextInput in nextInputs:
                                    nextInput[inputKey] = self.__dict__[block.__dict__[inputKey].outputKey]
                            
                        elif self.outputs[block.__dict__[inputKey].outputKey] == "parallel":
                            temp = []
                            
                            # Every nextInput needs to be duplicated for every items of
                            # the parallel output.
                            for item in self.__dict__[block.__dict__[inputKey].outputKey]:
                                if nextInputs is None : 
                                    temp = [{inputKey:item}]
                                else:                                
                                    for nextInput in nextInputs:
                                        copyNI = deepcopy(nextInput)
                                        copyNI[inputKey] = item
                                        temp.append(copyNI)
                            nextInputs = temp
                        
                        else:
                            self.print_("Unknown output type.")                                   
                            raise NameError
                        
                    else: # there is no corresponding output
                        self.print_("No output/input correspondance: " + \
                                          block.__dict__[inputKey].outputKey +\
                                         " is not in available outputs " + str(self.outputs))   
                        raise NameError 
         

            if nextInputs is None :  
                #block.setEntryPoint(False)
                self.stageBlock(block)                          
                self.print_("Staging block: " + block.getSignature())                      
            else :   
                for nextInput in nextInputs:
                    # A thread can only be started once so we need to copy
                    # it as many times as required. 
                    newblock = deepcopy(block)
                    newblock.fromPreviousOutput = nextInput                           
                    #newblock.setEntryPoint(False)

                    self.stageBlock(newblock)      
                    self.print_("Staging block: " + newblock.getSignature())                          



                

        # This error occurs when the scheduler has been closed. There is no
        # action to take as the wanted behavior in this case is for the current
        # process to end, which happends at the end of this function.
        #except Pyro4.errors.ConnectionClosedError:
        #    pass            
        #except Pyro4.errors.CommunicationError:
        #    pass            
        #except:
        #    self.print_("Error running : " + self.getSignature())
        #    if self.processAvailable:
        #        self.scheduler.releaseProcess()
        #    raise

        

"""
 Class used to identified the BWM input variables as comming form a previous
 block.
"""
class fromPrevious:
    def __init__(self, outputKey):
        self.outputKey = outputKey






