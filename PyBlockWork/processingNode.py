# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 15:13:44 2013

@author: oreichri
"""

import socket

import sys, time, signal, traceback
from multiprocessing import Process
import Pyro4
import os
from cStringIO import StringIO

from spyndle import terminate_process
from .block import StagingRoomWrapper
from .pyroWarehouse import warehouse


###############################################################################
# Framework objects
###############################################################################



class ProcessingNode:
    def __init__(self, nbWorkers, stagingRoomName="stagingRoom", 
                         comHubName="communicationHub", HMAC_KEY=""):
                                     
        self.ip = socket.gethostbyname(socket.gethostname())
        self.workerPool = WorkerPool(nbWorkers, self.ip, stagingRoomName, 
                                                         comHubName, HMAC_KEY)

    def terminate(self, force=False):
        self.workerPool.requestStop(force) 


class WorkerPool:
    def __init__(self, nbWorkers, ip, stagingRoomName, comHubName, HMAC_KEY):
                                             
        Pyro4.config.HMAC_KEY = HMAC_KEY           
        sys.excepthook = Pyro4.util.excepthook    
        
        try:
            Pyro4.locateNS()
        except Pyro4.errors.PyroError, e:
            print 'Pyro error message: ' + str(e)
            raise
        except:
            raise        

        self.comHubName = comHubName

        # Watch dog to stop the worker processes if the registered pyros objects
        #  becomes unavailable.Ensure that no orphan process stays alive
        # upon termination of mandatory pyro objects.
        self.watchdog = WhatchDog(HMAC_KEY)  
        self.watchdog.daemon = True  
        
        self.workers = {}
        for i in range(nbWorkers):
            workerID = ip + "-" + str(i)
            self.workers[workerID] = BlockWorker(workerID, stagingRoomName, 
                                            comHubName, HMAC_KEY, self.watchdog)
            self.workers[workerID].start()
            self.watchdog.registerProcess(self.workers[workerID].pid)

        
        self.watchdog.registerMonitoredObjects(stagingRoomName)
        self.watchdog.registerMonitoredObjects(comHubName)
        
        self.watchdog.start()                


    @property
    def comHub(self):
        return warehouse.proxy(self.comHubName) 

    def requestStop(self, force=False):

        for workerID in self.workers:
            self.comHub.terminateWorker(workerID)            

            self.workers[workerID].stopRequested = True
            if force:
                 os.kill(self.workers[workerID].pid, signal.SIGINT)
                 


"""
 Wrapping a process around the BWModule object because we don't want to be passing
 arround process objects. We want to be able to pass BWModule and to constitute
 this process object only when the BWModule run() method must be executed thought
 as start() call. 
"""
class BlockWorker(Process):

    def __init__(self, ID, stagingRoomName, comHubName, HMAC_KEY, watchdog):
        
        super(BlockWorker, self).__init__()
        self.ID                 = ID
        self.stagingRoomName    = stagingRoomName
        self.comHubName         = comHubName
        self.HMAC_KEY           = HMAC_KEY
        self.watchdog           = watchdog


        # Set the signal handler to catch process interuption process
        if sys.platform == 'win32':
            signal.signal(signal.SIGINT, self.exitProcess)
            signal.signal(signal.SIGTERM, self.exitProcess)
            signal.signal(signal.SIGABRT, self.exitProcess)
            
        # TODO: Add specific set of defined signals for the various platform
        else:
            signal.signal(signal.SIGINT, self.exitProcess)
            signal.signal(signal.SIGTERM, self.exitProcess)
            signal.signal(signal.SIGABRT, self.exitProcess)
            signal.signal(signal.SIGQUIT, self.exitProcess)
            signal.signal(signal.TSTP, self.exitProcess)
            signal.signal(signal.CTRL_C_EVENT, self.exitProcess)
            
        self.stopRequested = False
    
    
    @property
    def comHub(self):
        return warehouse.proxy(self.comHubName)   
    
    def print_(self, string):
        try:
            self.comHub.writeMessage(self.ID, string)  
        except Pyro4.errors.NamingError:
            nameserver = Pyro4.naming.locateNS()
            print "---------------------------------------------------------- "            
            print "Name server know objects: "
            print "--------START LIST " 
            for name, uri in sorted(nameserver.list().items()):
                print "%s --> %s" % (name, uri)
            print "--------END LIST" 
            raise
        except:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback())     
            raise
    
    
    def run(self): 

    
        Pyro4.config.HMAC_KEY = self.HMAC_KEY           
        sys.excepthook = Pyro4.util.excepthook    
        self.comHub.addWorker(self.ID) # This cannot be in the try block because
                                       # the print_ method needs the worker to be added

        try:
            self.stagingRoom = StagingRoomWrapper(self.stagingRoomName)

            while not self.stopRequested:

                """
                 This call returns a blockString if there is one in the staging room.
                 Else, it waits for 1 seconds for new blocks. If no block become
                 available during that time, it returs a None value. In this case we loop
                 to verify potential stop requests.
                """
                self.BWMobj = self.stagingRoom.getReadyBlock(True, 1)
                if self.BWMobj is None:
                    continue
                
                self.watchdog.registerMonitoredObjects(self.BWMobj.registerName)                
                self.BWMobj.run(self.ID, self.BWMobj.registerName, 
                                    self.stagingRoomName, self.comHubName)
                self.watchdog.unregisterMonitoredObjects(self.BWMobj.registerName)                
                    
        except:
            tracebackOutput = StringIO()
            traceback.print_exc(file=tracebackOutput)
            self.print_(tracebackOutput.getvalue())
            sys.exit(1)



    def exitProcess(self):
        self.stopRequested = True
        self.print_("Exit signal has been catched.")
       
       
        
        
        

class WhatchDog(Process):
    
    def __init__(self, HMAC_KEY):
        self.clientPIDs        = []  
        self.monitoredObjects  = []
        Process.__init__(self)
        self.HMAC_KEY = HMAC_KEY
        
    def run(self):
        
        Pyro4.config.HMAC_KEY= self.HMAC_KEY
        while(True):
            time.sleep(5)
            try:
                   
                for objName in self.monitoredObjects:
                    warehouse.proxy(objName).test()
                
            except Pyro4.errors.ConnectionClosedError:
                for PID in self.clientPIDs :
                    terminate_process(PID)
                break
            except Pyro4.errors.CommunicationError:
                for PID in self.clientPIDs :
                    terminate_process(PID)
                break


    def registerProcess(self, pid):
        if not pid in self.clientPIDs :
            self.clientPIDs.append(pid)

    def registerMonitoredObjects(self, objectName):
        if not objectName in self.monitoredObjects :
            self.monitoredObjects.append(objectName)
        
    def unregisterMonitoredObjects(self, objectName):
        if not objectName in self.monitoredObjects :
            self.monitoredObjects.remove(objectName)
        
        
        