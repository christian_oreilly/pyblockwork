# -*- coding: utf-8 -*-
"""
Created on Tue Jun 04 22:59:38 2013

@author: oreichri
"""



from Pyro4 import threadutil, naming
import Pyro4
from threading import Thread

class PyroServer(threadutil.Thread):
    def __init__(self, host, isDeamon, port=0, enableBroadcast=True, 
                 bchost=None, bcport=None, unixsocket=None, nathost=None, natport=None):

        super(PyroServer,self).__init__()
        
        """
         Start a name server
        """
        self.setDaemon(isDeamon)
        self.host=host
        self.started=threadutil.Event()
        self.unixsocket = unixsocket
        self.port = port
        self.enableBroadcast = enableBroadcast 
        self.bchost = bchost
        self.bcport = bcport
        self.nathost = nathost
        self.natport = natport       

        # This code is taken from Pyro4.naming.startNSloop
        self.ns_daemon = naming.NameServerDaemon(self.host, self.port, self.unixsocket, 
                                                 nathost=self.nathost, natport=self.natport)
        self.uri    = self.ns_daemon.uriFor(self.ns_daemon.nameserver)
        internalUri = self.ns_daemon.uriFor(self.ns_daemon.nameserver, nat=False)
        self.bcserver=None
        
        self.ns = self.ns_daemon.nameserver 
        
        if self.unixsocket:
            hostip = "Unix domain socket"
        else:
            hostip = self.ns_daemon.sock.getsockname()[0]
            if hostip.startswith("127."):
                self.enableBroadcast=False
            if self.enableBroadcast:
                # Make sure to pass the internal uri to the broadcast responder.
                # It is almost always useless to let it return the external uri,
                # because external systems won't be able to talk to this thing anyway.
                self.bcserver=naming.BroadcastServer(internalUri, self.bchost, self.bcport)
                self.bcserver.runInThread()


        """
         Start a pyro object server.
        """      
        self.pyroObjectServer = Pyro4.Daemon(self.host)                 # make a Pyro daemon
            
        thread = Thread(target = self.pyroObjectServer.requestLoop)
        thread.setDaemon(True)
        thread.start()



    def registerObject(self, obj, pyroName):        
        uri=self.pyroObjectServer.register(obj)   # register the greeting object as a Pyro object
        self.ns.register(pyroName, uri)  # register the object with a name in the name server
    



    def run(self):
        try:
            self.ns_daemon.requestLoop()
        finally:
            self.ns_daemon.close()
            if self.bcserver is not None:
                self.bcserver.close()

        
    def startNS(self):
        self.start()
        
    def stopNS(self):
        self.ns_daemon.shutdown()
        if self.bcserver is not None:
            self.bcserver.close()
        
        


# Helper function
def startNameServer(host, isDeamon):
    ns=PyroServer(host, isDeamon)
    ns.startNS()
    return ns


