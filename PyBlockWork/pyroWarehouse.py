# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 15:02:26 2013

@author: oreichri
"""

import Pyro4

class PyroWarehouse:
    """
     We used to use the "with" statement for accessing proxy objects. For example:
     
        try:
            with Pyro4.Proxy("PYRONAME:" + self.registerName) as register:   
                return register.getRegisterRepr()

        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback()) 
            raise
            
     this approach was used as a good programming practice. However, it turns 
     out that for some reasons (looks like a bug in the socket management in 
     Pyro4 when used in Windows) this structure results in three simultaneous 
     _pyroInvoke(...) call and probably create some kind of lock which produce 
     waiting time of about 1 s each time. Thus, we prefere to get the proxy at 
     creation as soon as they are required and keep them alive thougouth the 
     life cycle of the program. However, Proxy on pyro objects cannot always
     be keep in the user class. This is the case when the user class is itself
     a Pyro object. In these cases, keeping a proxy as a class member resuls in
     runtime error since this proxy class cannot be serialized through the 
     serpent serializer.
     
     The best solution found so far is to create a global PyroWarehouse object
     that create the proxy as soon as they are needed and keep them alive to 
     provide them to their users whenever they need them.         
    """
    
    def __init__(self):
        
        self.warehouse = {}
        
    def proxy(self, URI):

        try:
            if not URI in self.warehouse :
                self.warehouse[URI] = Pyro4.Proxy("PYRONAME:" + URI) 
            return self.warehouse[URI]
    
        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback()) 
            raise                
        
        
    def isValid(self, URI):
        try:
            if not URI in self.warehouse :
                self.warehouse[URI] = Pyro4.Proxy("PYRONAME:" + URI) 
                
            self.warehouse[URI].test()
            return True

        except Exception:
            return False     


global warehouse
warehouse = PyroWarehouse()
            