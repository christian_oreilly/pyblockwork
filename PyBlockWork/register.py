# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""


from cPickle import dump, load
import os
from multiprocessing import Semaphore


class RegisterItem:
    def __init__(self):
        self.hasStarted  = False
        self.hasExecuted = False
        self.isInError   = False
        self.blockString = ""
        
    def __repr__(self):
        return str({"blockString":self.blockString, 
                    "hasStarted":self.hasStarted,
                    "isInError":self.isInError,
                    "hasExecuted":self.hasExecuted})
        
    def __str__(self):
        return self.__repr__()    
    
 

class Register:
    """
     Class used to register the state of execution of the various blocks. Each
     schema could (and probably should) use a different register object.
    """


    def __init__(self, pickleFileName = "C:/temp.bin"):
        
        # Make sure that different threads do not corrupt the register
        # by accessing it simultaneously.
        self.registerSempaphore = Semaphore(1)   

        self.pickleFileName = pickleFileName
              
        if os.path.isfile(pickleFileName) : 
            with open(pickleFileName, "rb") as pickleFile :
                self.register = load(pickleFile)
        else:
            self.register = {}


    def __repr__(self):
        representation = {}
        for key, value in self.register.iteritems():
            representation[key] = str(value)
        return str(representation)
        

    def getRegisterRepr(self):
        return self.__repr__()        

    def registerExecutionStart(self, blockID):

        with self.registerSempaphore :  
            if not blockID in self.register:
                self.register[blockID] = RegisterItem()                   
            self.register[blockID].hasStarted = True     
            # As a new execution is started, the error flag from the last 
            # execution is reset.
            self.register[blockID].isInError  = False
                        
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        
        
        
        
    def registerExecutionCompletion(self, blockID, outputDict):

        with self.registerSempaphore :   
            if not blockID in self.register:
                self.register[blockID] = RegisterItem()                   
            self.register[blockID].hasExecuted = True
            self.register[blockID].outputDict  = outputDict
                
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        

    def registerExecutionError(self, blockID): 
        
        with self.registerSempaphore :   
            if not blockID in self.register:
                self.register[blockID] = RegisterItem()                   
            self.register[blockID].isInError = True
                
            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        




    def hasExecuted(self, blockID):
 
        with self.registerSempaphore :    
                      
            if blockID in self.register:        
                retVal = self.register[blockID].hasExecuted
            else:
                retVal = False
        
        return retVal



    def resetStatus(self, blockID):
                
        with self.registerSempaphore :   
            if not blockID in self.register:
                self.register[blockID] = RegisterItem()                   
            self.register[blockID].isInError = False
            self.register[blockID].hasStarted = False
            self.register[blockID].hasExecuted = False

            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        


    def updateStatus(self, blockID):
        """
         Ensure that not out-dated status are kept. This can happen for example
         for the isInError and the hasStarted flag has been raised in a previous
         execution but the hasExecuted flag has not been raised.
        """
        with self.registerSempaphore :   
            if not blockID in self.register:
                self.register[blockID] = RegisterItem()    
            
            if self.register[blockID].hasExecuted == False:
                self.register[blockID].hasStarted = False
                self.register[blockID].isInError = False

            # save the modifications
            with open(self.pickleFileName, "wb") as f: 
                dump(self.register, f)
        
        


        
        
        

    def isInError(self, blockID):

        with self.registerSempaphore :    
                      
            if blockID in self.register:        
                retVal = self.register[blockID].isInError
            else:
                retVal = False
        
        return retVal


    def hasStarted(self, blockID):
 
        with self.registerSempaphore :    
                      
            if blockID in self.register:        
                retVal = self.register[blockID].hasStarted
            else:
                retVal = False
        
        return retVal



    def getOutputDict(self, blockID):
        with self.registerSempaphore :         
                 
            if blockID in self.register:        
                return self.register[blockID].outputDict
            else:
                raise ValueError("Block ID " + str(blockID) + " is not registered.\n"\
                                 "Content of the register: " + str(self))


   
    '''   
    def getBlockStrings(self):
        """
         As of version 4.23, we cannot use direct attribute access or direct
         property access with Pyro. We need to use a method.
        """
        return self.blockStrings             
    '''    

        
        
    def registerBlockString(self, ID, blockString, overwrite=False):
        if not overwrite:
            if ID in self.register:
                raise ValueError("The block with ID " + str(ID) + 
                                 " has already been registered. To update its " + 
                                 "blockString, use the updateBlockString method.")
                         
        with self.registerSempaphore :  
            if not ID in self.register:
                self.register[ID] = RegisterItem()                
            self.register[ID].blockString = blockString
        
        

    def updateBlockString(self, ID, blockString):
        if not ID in self.register:
            raise ValueError("The block with ID " + str(ID) + 
                             " is not registered. To register it, " + 
                             "use the registerBlockString method.")

        with self.registerSempaphore :  
            if not ID in self.register:
                self.register[ID] = RegisterItem()                
            self.register[ID].blockString = blockString
           

    def getBlockString(self, ID):
        if not ID in self.register:
            raise ValueError("The block with ID " + ID + " has not been registered.")
        return self.register[ID].blockString




    def blockIsRegistered(self, ID):
        return ID in self.register

    def test(self): pass


