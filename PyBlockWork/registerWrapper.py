# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 14:57:02 2013

@author: oreichri
"""

from .register import Register
from .pyroWarehouse import warehouse



def getBlock(ID, registerName):
    """
     Helper function to get a block with ID that is registered in the register
     named registerName.
    """    

    blockString = warehouse.proxy(registerName).getBlockString(ID)
     
    
    from .block import Block
    return Block.deserialize(blockString)           

class RegisterWrapper:
    """
     Class used wrapping the Register class. The goal of this object if class
     is to provide a local interface to remote objects.

     This class allows to simplify the management of blocks. To simplify the
     serialization, block objects don't contain references to other blocks but
     rather identification strings associated with other blocks. However, 
     getting string representation of blocks is not practical because we have 
     then to reconstruct the block from the reprensentation. These complications
     are managed by this wrapper such that we can interact with the staging room
     more convivialy. 
     
     Note: This class cannot be implemented directly in the register module 
     because this  creates cyclical imports of module block and register.     
    """

    
    def __init__(self, registerName):
        self.registerName = registerName

        
        
    @property
    def register(self):
        return warehouse.proxy(self.registerName).getRegisterRepr()

        
    def getBlock(self, blockID):
        blockString = self.registerExec(Register.getBlockString, blockID)
        from .block import Block
        return Block.deserialize(blockString)        
    
    def registerBlock(self, block, overwrite=False):
        self.registerExec(Register.registerBlockString, block.ID, block.serialize(), overwrite)    
    
    def updateBlock(self, block):
        self.registerExec(Register.updateBlockString, block.ID, block.serialize())            

    def isRegistered(self, block):    
        return self.registerExec(Register.blockIsRegistered, block.ID)        
    
    def hasExecuted(self, block = None, ID  = None):    
        
        if not block is None:                     
            return self.registerExec(Register.hasExecuted, block.ID)     
        elif not ID is None :         
            return self.registerExec(Register.hasExecuted, ID)   
        else:
            raise ValueError("A block or a block ID must be passed.")
    
    def hasStarted(self, block = None, ID  = None):    
        
        if not block is None:                     
            return self.registerExec(Register.hasStarted, block.ID)     
        elif not ID is None :         
            return self.registerExec(Register.hasStarted, ID)   
        else:
            raise ValueError("A block or a block ID must be passed.")    
    
    def isInError(self, block = None, ID  = None):    
        
        if not block is None:                     
            return warehouse.proxy(self.registerName).isInError(block.ID)
        elif not ID is None :         
            return warehouse.proxy(self.registerName).isInError(ID)
        else:
            raise ValueError("A block or a block ID must be passed.")    
    

    
    def resetStatus(self, block = None, ID  = None):    
        
        if not block is None:                     
            return warehouse.proxy(self.registerName).resetStatus(block.ID)
        elif not ID is None :         
            return warehouse.proxy(self.registerName).resetStatus(ID)
        else:
            raise ValueError("A block or a block ID must be passed.")   
    
    
    def updateStatus(self, block = None, ID  = None):    
        
        if not block is None:                     
            return warehouse.proxy(self.registerName).updateStatus(block.ID)
        elif not ID is None :         
            return warehouse.proxy(self.registerName).updateStatus(ID)
        else:
            raise ValueError("A block or a block ID must be passed.")    

    
    def getOutputDict(self, block):    
        return self.registerExec(Register.getOutputDict, block.ID)            
    
    def registerExecutionStart(self, block):                    
        return self.registerExec(Register.registerExecutionStart, block.ID)            
    
    def registerExecutionCompletion(self, block, outputDict):                 
        return self.registerExec(Register.registerExecutionCompletion, block.ID, outputDict)            
    
    def registerExecutionError(self, block):
        warehouse.proxy(self.registerName).registerExecutionError(block.ID)
    
    
    def registerExec(self, func, *args, **kargs):

        if func == Register.hasStarted:
            return warehouse.proxy(self.registerName).hasStarted(args[0])
        elif func == Register.hasExecuted:
            return warehouse.proxy(self.registerName).hasExecuted(args[0])
        elif func == Register.getOutputDict:
            return warehouse.proxy(self.registerName).getOutputDict(args[0])  
        elif func == Register.registerExecutionStart:
            warehouse.proxy(self.registerName).registerExecutionStart(args[0])  
        elif func == Register.registerExecutionCompletion:
            warehouse.proxy(self.registerName).registerExecutionCompletion(args[0], args[1])   
        elif func == Register.registerBlockString:
            warehouse.proxy(self.registerName).registerBlockString(args[0], args[1], args[2])
        elif func == Register.getBlockString:
            return warehouse.proxy(self.registerName).getBlockString(args[0])
        elif func == Register.updateBlockString:
            warehouse.proxy(self.registerName).updateBlockString(args[0], args[1])
        elif func == Register.blockIsRegistered:
            return warehouse.proxy(self.registerName).blockIsRegistered(args[0])
        else:
            raise ValueError("Unknow function" + str(func))                    
                

        
        
    def isValid(self):
        return warehouse.isValid(self.registerName)
    
    
    
        
        