# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 17:27:00 2013

@author: oreichri
"""

import serpent
import collections

from .block import Block
from .blocks import *
from .pyroWarehouse import warehouse

from .block import StagingRoomWrapper
from .registerWrapper import RegisterWrapper

class Schema(collections.MutableMapping):
    

    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys
        
        self.__startBlock       = None
        self.__registerName     = "register"
        self.__HMAC_KEY         = ""
        self.__stagingRoomName  = "stagingRoom"





    @property
    def HMAC_KEY(self):
        return self.__HMAC_KEY

    @HMAC_KEY.setter
    def HMAC_KEY(self, value):
        if not isinstance(value, str):
            raise TypeError("HMAC_KEY property must be a str. A variable of type "\
                                                + type(value) + " has been passed.")
        self.__HMAC_KEY = value


    @property
    def stagingRoomName(self):
        return self.__stagingRoomName
        
    @stagingRoomName.setter
    def stagingRoomName(self, stagingRoomName):
        
        if not self.startBlock is None:
            self[self.__startBlock].stagingRoomName = stagingRoomName        
        
        self.__stagingRoomName = stagingRoomName



    @property
    def registerName(self):
        return self.__registerName 
        
    @registerName.setter
    def registerName(self, registerName):
        
        if not self.startBlock is None:
            self[self.__startBlock].registerName = registerName        
        
        self.__registerName = registerName
        
    
    @property
    def startBlock(self):
        return self.__startBlock 

    @startBlock.setter
    def startBlock(self, name):
        if name in self:
            self.__startBlock = name
            self[name].registerName = self.__registerName
        else:
            raise ValueError("Cannot set " + name + " as the starting block because the schema has no such block.")


    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        value.name = self.__keytransform__(key)
        self.store[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]
        
    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key


    def getBlock(self, name=None, ID=None):
        if not name is None:
            return self.store[name]
        elif not ID is None:
            ID_dict = {v.ID:v for k, v in self.store.items()}
            return ID_dict[ID]

    
    
    
    def start(self, stagingRoomName, registerName=None, HMAC_KEY=None, verbose = False):
        
        if verbose:
            print "Starting schema..."
        
        self.__stagingRoomName = stagingRoomName
        self.stagingRoom = StagingRoomWrapper(stagingRoomName)               
            
        if self.startBlock is None:
            raise ValueError("Cannot start the schema because it has no starting block. First set one using the startBlock property.")            
          
        if not HMAC_KEY is None:
            self.HMAC_KEY = HMAC_KEY
        
        for key in self.store:
            self.store[key].HMAC_KEY = self.HMAC_KEY
            
        if not registerName is None:
            self.registerName = registerName
            for key in self.store:
                self.store[key].registerName = self.registerName
        else:
            raise NameError("The registerName argument must not be none "\
                            "if schema.registerName has been left to None.")                
        self.register = RegisterWrapper(self.registerName)
     
        
        
        for key in self.store:
            self.store[key].stagingRoomName = self.stagingRoomName
            self.register.registerBlock(self.store[key], overwrite=True)   
            # Ensure that there is no status left form previous incomplete 
            # processing.
            self.register.updateStatus(self.store[key])            
            
        if verbose:
            print "staging block " + self[self.startBlock].ID
            
        self.stagingRoom.stageBlock(self[self.startBlock])






    def save(self, fileName) :
        serpent.dump(self, open(fileName, "wb" ), indent=True, set_literals=False)
        
        
    @staticmethod
    def load(fileName) :

        serpentData = serpent.load(open(fileName, "rb"))
        
        if not serpentData["__class__"] == "Schema" :
            raise ValueError("The code contained in " + fileName + " is invalid.")
             
        schema = Schema()        
        
        schema.__startBlock = serpentData["_Schema__startBlock"]
        schema.__registerName = serpentData["_Schema__registerName"]
        schema.store = serpentData["store"]

        for key in schema.store:    
            schema.store[key] = Block.fromASTtoObject(schema.store[key])

        return schema
        
        