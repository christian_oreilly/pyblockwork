# -*- coding: utf-8 -*-
"""
Created on Wed May 29 14:14:39 2013

@author: REVESTECH
"""

import multiprocessing as mp
import Queue

from .registerWrapper import getBlock
from .pyroWarehouse import warehouse



class StagingRoomWrapper:
    """
     Class used wrapping the StagingRoom class. The goal of this object if class
     is to provide a local interface to remote objects.
     
     This class allows to simplify the management of blocks. To simplify the
     serialization, block objects don't contain references to other blocks but
     rather identification strings associated with other blocks. Also, the staging 
     room do not store blocks but string representations of blocks. However, 
     getting string representation of blocks is not practical because we have 
     then to reconstruct the block from the reprensentation. These complications
     are managed by this wrapper such that we can interact with the staging room
     more convivialy. 
    """
    
    def __init__(self, stagingRoomName):
        self.stagingRoomName = stagingRoomName

        
    def getReadyBlock(self, blocking=True, timeout=1.0):
        if self.stagingRoomName is None:
            raise Exception("The staging room name is set to None. Connection to the staging room not possible.")
        
        result = warehouse.proxy(self.stagingRoomName).getReadyBlock(True, 1)
        if result is None:
            return None
        blockID, registerName = result
        return getBlock(blockID, registerName)

        

    """
    @property
    def blockStrings(self):
        if self.stagingRoomName is None:
            raise Exception("The staging room name is set to None. Connection to the staging room not possible.")
        
        try:
            with Pyro4.Proxy("PYRONAME:" + self.stagingRoomName) as stagingRoom:   
                return stagingRoom.getBlockStrings() #{k:v for k,v in stagingRoom.blockStrings.iteritems()}

        except Exception:
            print "Pyro traceback:"
            print "".join(Pyro4.util.getPyroTraceback()) 
            raise
    """
        
            
    @property
    def waitingBlocks(self):
        if self.stagingRoomName is None:
            raise Exception("The staging room name is set to None. Connection to the staging room not possible.")

        return warehouse.proxy(self.stagingRoomName).getWaitingBlocks() #{k:v for k,v in stagingRoom.blockStrings.iteritems()}

    
    def changeMode(self, mode):
        warehouse.proxy(self.stagingRoomName).changeMode(mode)
    
    
    def hasBeenStaged(self, block):
        return warehouse.proxy(self.stagingRoomName).hasBeenStaged(block.ID)        
               
    
    def stageBlock(self, block):

        if self.stagingRoomName is None:
            raise Exception("The staging room name is set to None. Connection to the staging room not possible.")
        
        warehouse.proxy(self.stagingRoomName).addReadyBlock(block.ID, block.registerName, block.depth)



        






class StagedBlock:
    def __init__(self, blockID, registerName):
        self.blockID        = blockID
        self.registerName   = registerName
        
        
class StagingRoom:
    """
     Class used to put blocks in an execution queue. There should be only one
     staging queue by running instance of the central processing program, such
     that many schema can be ran in parrallel by a same program. Only the block
     IDs are stored in this object. A Register object must be called to obtain
     more information about the staged blocks.
    """
    
    
    
    def __init__(self):
        self.mode               = "vertical"
        self.waitingBlockQueues = []
        
        # A list of all blockIDs that have been staged.
        self.stagedBlocks   = []
        

    def hasBeenStaged(self, blockID) : 
        return blockID in self.stagedBlocks  
        
        
    def changeMode(self, mode) :
        if mode == "vertical" or self.mode == "horizontal":
            self.mode = mode
        else:
            raise ValueError("Mode can only take two values: 'vertical' or 'horizontal'.")



        
        
    def getWaitingBlocks(self):
        """
         As of version 4.23, we cannot use direct attribute access or direct
         property access with Pyro. We need to use a method.
        """
        
     
        IDList = []
        for queue in self.waitingBlockQueues:
            keepLooping = True   
            blockList = []
            while keepLooping:
                try:
                    blockList.append(queue.get(False))
                    IDList.append(blockList[-1].blockID)
                except Queue.Empty:
                    keepLooping = False
                    
            for block in blockList:
                queue.put(block)
                
        return IDList
        
        
        
    def addReadyBlock(self, blockID, registerName, priority):
        self.stagedBlocks.append(blockID)
        if priority >= 20:
            raise ValueError("Maximal depth has been set to 20 for schema blocks.")
        while priority >= len(self.waitingBlockQueues):
            self.waitingBlockQueues.append(mp.Queue())
            
        self.waitingBlockQueues[priority].put(StagedBlock(blockID, registerName))
              
              
    def getReadyBlock(self, blocking=True, timeout=1.0):
        """
         The Serpent algorithm cannot serialize the Queue.Empty class. We must
         therefore catch the exception and indicate the failure by returning
         a None value.
        """ 
        if self.mode == "vertical":
            inds = range(len(self.waitingBlockQueues)-1, -1, -1)
        else:
            inds = range(len(self.waitingBlockQueues))
        for ind in inds:
  
            try:
                stagedBlock = self.waitingBlockQueues[ind].get(blocking, timeout)  
                return stagedBlock.blockID, stagedBlock.registerName            
            except Queue.Empty:
                pass        
        
        return None



    def test(self): pass
