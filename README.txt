PyBlockWork offers a framework for defining signal processing operations as modules that can be executed in a distributed environment.

Please consult https://bitbucket.org/christian_oreilly/pyblockwork/wiki/Home for more information.